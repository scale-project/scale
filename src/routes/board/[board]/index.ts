import { api } from '../_api';
import { post_img, get_img, get_img_type } from '$lib/work_with_images';
import type { RequestHandler } from '@sveltejs/kit';
import type { Thread } from 'src/models/Thread';
import { writeFileSync, existsSync } from 'fs';
import { v4 as GUIDv4 } from 'uuid';

export const get: RequestHandler = async ({ params }) => {
  // locals.userid comes from src/hooks.js
  const response = await api('get', `board/${[params.board]}`);
  if (response.status === 404) {
	return {
	  body: {
		threads: null,
		board: null
	  }
	};
  }

  if (response.status === 200) {
	// all this is to see if the image is saved in the filesystem
	// or it needs to use the api to get it from the other storage
	let obj = await response.json();
	let threads: Thread[];
	threads = obj;
	if(obj != '')
	{
	  for(let i = 0; i < threads.length; i++)
	  {
		let imageId: string = threads[i].imageId;
		if(imageId != null) {
		  if(existsSync(`static/images/${imageId}`))
		    threads[i].imageId = `/images/${imageId}`;
		  else
		    threads[i].imageId = await get_img(imageId);
        }

        threads[i].threadText = replaceURLs(threads[i].threadText, params.board);
	  }
	} else threads = null;

	return {
	  body: {
		threads: threads,
		board: params.board
	  }
	};
  }

  return {
	status: response.status
  };
};

export const post: RequestHandler = async ({ request, params, locals }) => {
  const form = await request.formData();
  let base64image = form.get("base64image").toString();
  let data = {};
  data["id"]= GUIDv4().toString(); // gonna be set in api
  data["ThreadName"] = form.get("ThreadName").toString();
  data["ThreadCreator"] = locals.userid;
  data["ThreadText"] = form.get("ThreadText").toString();
  data["Comments"] = [];
  data["CreationDate"] = Date.now();
  data["ImageId"] = await post_img(base64image, writeFileSync);
  data["FileType"] = get_img_type(base64image);

  await api('post', `thread/${params.board}`, JSON.stringify(data));

  return {};
};

function replaceURLs(text: string, board: string): string {
  if(!text) return;

  // link to thread on other board
  let otherthreadlinkRegex = />>>\/.*\/[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}/g;
  text = text.replace(otherthreadlinkRegex, (id) => {
    let link = id.slice(4);
    let parts = link.split('/');
    let linkid = parts[1]; let board = parts[0];
    return ` <a href="${board}/${linkid}#{linkid}"> >>>/${board}/${linkid} </a> `
  });

  // link to thread on this board
  let threadlinkRegex = />>>[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}/g;
  text = text.replace(threadlinkRegex, (id) => {
	let linkid = id.slice(3); // remove the first >>>
	return ` <a href="${board}/${linkid}#${linkid}"> ${id} </a> `
  });
  // link to another board
  let otherboardRegex = />>>\/.*\/($|(^[0-9a-fA-F]))/g;
  text = text.replace(otherboardRegex, (id) => {
    let boardid = id.split('/')[1];
    return ` <a href="${boardid}"> >>>/${boardid}/ </a> `
  });

  let newlineRegex = /(\r\n|\r|\n)/g;
  text = text.replace(newlineRegex, () => {
	return ' <br> '
  });

  let urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
  return text.replace(urlRegex, (url) => {
	var hyperlink = url;
	if (!hyperlink.match('^https?:\/\/')) {
	  hyperlink = 'http://' + hyperlink;
	}
	return ' <a href="' + hyperlink + '" target="_blank" rel="noopener noreferrer">' + url + '</a> '
  });
}
