import { api } from '../_api';
import { post_img, get_img, get_img_type } from '$lib/work_with_images';
import type { RequestHandler } from '@sveltejs/kit';
import type { Thread } from 'src/models/Thread';
import { writeFileSync, existsSync } from 'fs';
import { v4 as GUIDv4 } from 'uuid';

export const get: RequestHandler = async ({ params }) => {
  // locals.userid comes from src/hooks.js
  const response = await api('get', `thread/${[params.board]}/${[params.tid]}`);
  if (response.status === 404) {
	return {
	  body: {
		thread: null,
		board: null
	  }
	};
  }

  if (response.status === 200) {
	// all this is to see if the image is saved in the filesystem
	// or it needs to use the api to get it from the other storage
	let obj = await response.json();
	let thread: Thread;
	thread = obj;
	if(obj != '')
	{
	  let imageId: string = thread.imageId;
	  if(imageId != null)
	  {
		if(existsSync(`static/images/${imageId}`))
		  thread.imageId = `/images/${imageId}`;
		else
		  thread.imageId = await get_img(imageId);
	  }

      thread.threadText = replaceURLs(thread.threadText, params.tid, params.board);

	  for(let i = 0; i < thread.comments.length; i++)
	  {
		imageId = thread.comments[i].imageId;
		if(existsSync(`static/images/${imageId}`))
		  thread.comments[i].imageId = `/images/${imageId}`
		else
		  thread.comments[i].imageId = await get_img(imageId);

        thread.comments[i].commentText = replaceURLs(thread.comments[i].commentText, params.tid, params.board);
	  }
	} else thread = null;

	return {
	  body: {
		thread: thread,
		board: params.board
	  }
	};
  }

  return {
	status: response.status
  };
};

export const post: RequestHandler = async ({ request, params, locals }) => {
  const form = await request.formData();
  let base64image = form.get("base64image").toString();
  let data = {};
  data["id"] = data["id"] = GUIDv4().toString(); // gonna be set in api
  data["CommentCreator"] = locals.userid;
  data["CommentText"] = form.get("CommentText").toString();
  data["CreationDate"] = Date.now();
  data["ImageId"] = await post_img(base64image, writeFileSync);
  data["FileType"] = get_img_type(base64image);

  await api('post', `comment/${params.board}/${params.tid}`, JSON.stringify(data));

  return {};
};

function replaceURLs(text: string, OPtid: string, board: string): string {
  if(!text) return;

  let replyRegex = /(^|[^>])>>[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}/g;
  text = text.replace(replyRegex, (id) => {
	let showid = id;
	id = id.replace(/(>|\n| )/g, '');
	if(id == OPtid) showid += "(OP)"
	// this adds the targeted class
	// to the target id when its hovered on
	// and removes it when it stops hovering
	return ` <a class="${id}" href="#${id}" onmouseover="document.getElementById('${id}').classList.add('targeted')" onmouseleave="document.getElementById('${id}').classList.remove('targeted')"> ${showid} </a> `;
  });

  // link to thread on other board
  let otherthreadlinkRegex = />>>\/.*\/[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}/g;
  text = text.replace(otherthreadlinkRegex, (id) => {
    let link = id.slice(4);
    let parts = link.split('/');
    let linkid = parts[1]; let board = parts[0];
    return ` <a href="${board}/${linkid}#{linkid}"> >>>/${board}/${linkid} </a> `
  });
  // link to thread on this board
  let threadlinkRegex = />>>[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}/g;
  text = text.replace(threadlinkRegex, (id) => {
    let linkid = id.slice(3); // remove the first >>>
    return ` <a href="../${board}/${linkid}#${linkid}"> ${id} </a> `
  });
  // link to another board
  let otherboardRegex = />>>\/.*\/($|[^0-9a-fA-F])/g;
  text = text.replace(otherboardRegex, (id) => {
    let boardid = id.split('/')[1];
    return ` <a href="../${boardid}"> >>>/${boardid}/ </a> `
  });

  let newlineRegex = /(\r\n|\r|\n)/g;
  text = text.replace(newlineRegex, () => {
	return ' <br> '
  });

  let urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
  return text.replace(urlRegex, (url) => {
	var hyperlink = url;
	if (!hyperlink.match('^https?:\/\/')) {
	  hyperlink = 'http://' + hyperlink;
	}
	return ' <a href="' + hyperlink + '" target="_blank" rel="noopener noreferrer">' + url + '</a> '
  });
}
