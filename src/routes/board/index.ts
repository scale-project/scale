import { api } from './_api';
import type { RequestHandler } from '@sveltejs/kit';

export const get: RequestHandler = async () => {
	// locals.userid comes from src/hooks.js
	const response = await api('get', 'boards');

	if (response.status === 404) {
		// user hasn't created a todo list.
		// start with an empty array
		return {
			body: {
				info: 'balls'
			}
		};
	}

	if (response.status === 200) {
		return {
			body: {
				boards: await response.json()
			}
		};
	}

	return {
		status: response.status
	};
};
