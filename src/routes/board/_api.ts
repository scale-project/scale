const base = 'https://localhost:5001/api'

export function api(method: string, resource: string, data?: any)
{
    return fetch(`${base}/${method}/${resource}`, {
		method,
		headers: {
			'content-type': 'application/json'
		},
		body: data
	});
}
