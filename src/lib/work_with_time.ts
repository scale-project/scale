export function formatTime(timestamp: number): string
{
    let date = new Date(timestamp);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let time = date.toTimeString().split(' ')[0];
    let formattedTime = `${time}, ${day}/${month}/${year}`

    return formattedTime;
}