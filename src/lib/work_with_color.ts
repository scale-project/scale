export const creatorColor = (creatorid: string) => {
    let parts = creatorid.split('-')
    let ints = parts.map(function(d) { return parseInt(d,16) })
    let code = ints[0]

    let blue = (code >> 16) & 31;
    let green = (code >> 21) & 31;
    let red = (code >> 27) & 31;
    let foreColor = `border:5px solid rgb(${red << 3}, ${green << 3}, ${blue << 3});`;
    return foreColor;
};