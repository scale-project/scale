import { api } from '../routes/board/_api'
import { v4 as GUIDv4 } from 'uuid';

export async function post_img(img: string, writeFileFunc: Function) {
	if(img == null || img == '' || img == undefined) return null;

	img = img.split(':')[1];        // remove 'data:' part
	let img_b64= img.split(',')[1]; // remove header
	let fileExt = img.split(',')[0].split('/')[1].split(';')[0]; // get only the file extension
	let filename = `${GUIDv4().toString()}.${fileExt}`;

	//save file in this server 
	const file = img_b64;
	writeFileFunc(`static/images/${filename}`, file, "base64");

	let data = {};
	data["name"] = filename;
	data["base64"] = img_b64;
	await api('post', 'image', JSON.stringify(data));

	return filename;
}

export function get_img_type(img: string): string 
{
	if(img == null || img == '' || img == undefined) return null;
	return img.split('/')[0].split(':')[1];
}

export async function get_img(imgId: string) {
	if(imgId == null || imgId == '' || imgId == undefined) return null;

	const response = await api('get', `image/${imgId}`)

	if(response.status !== 200) return `Server error: ${response.status}`;
	return response.text();
}
