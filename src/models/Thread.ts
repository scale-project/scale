export type Comment = {
    id: string;
    commentCreator: string;
    commentText: string;
    creationDate: number;
    imageId: string;
    fileType: string //image or video
};

export type Thread = {
    id: string;
    threadName: string;
    threadCreator: string;
    threadText: string;
    comments: Comment[];
    creationDate: number;
    imageId: string;
    fileType: string; //image or video
};