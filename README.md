## Important

Dont use this for scale project ui, use [scale-astro](https://gitlab.com/scale-project/scale-astro)
its  better and faster, this readme is still valid for the project as a whole, just dont use this

### This is scale

Not much to say. Its an image board that uses svelte front end and dotnet backend with mongodb as database and google drive as image backup. Thats about it

The API: [https://gitlab.com/scale-project/scale-api](https://gitlab.com/scale-project/scale-api)
Tools for developing and deploying: [https://gitlab.com/scale-project/scale-tools](https://gitlab.com/scale-project/scale-tools)

### It supports:
- Uploading a thread
- Uploading an image with that thread
- Replying with an image
- mentioning other users
- id based on ip? (questionable feature)

### It wil support in the future:
- An easy way for mods to delete thread
- System for moderators
- More efficient and fast fetching of threads
- An algorithm to remove and change position of threads
- Better reply system (whatever that means)

### In far future it may support:
- Changing themes

## How to run
I use the `run.sh` file, it has a simple npm command, note its only for developemnt
If you want to deploy this webapp on your machine/server see [this](https://gitlab.com/scale-project/scale-tools/-/tree/main/manage-secrets)
